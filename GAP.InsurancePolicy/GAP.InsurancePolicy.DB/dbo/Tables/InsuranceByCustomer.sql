﻿CREATE TABLE [dbo].[InsuranceByCustomer](
	[InsuranceByCustomerID] [int] IDENTITY(1,1) NOT NULL,
	[CustomerID] [int] NOT NULL,
	[InsurancePolicyID] [int] NOT NULL,
 CONSTRAINT [PK_InsuranceByCustomer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC,
	[InsurancePolicyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[InsuranceByCustomer]  WITH CHECK ADD  CONSTRAINT [FK_InsuranceByCustomer_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
GO

ALTER TABLE [dbo].[InsuranceByCustomer] CHECK CONSTRAINT [FK_InsuranceByCustomer_Customer]
GO
ALTER TABLE [dbo].[InsuranceByCustomer]  WITH CHECK ADD  CONSTRAINT [FK_InsuranceByCustomer_InsurancePolicy] FOREIGN KEY([InsurancePolicyID])
REFERENCES [dbo].[InsurancePolicy] ([InsurancePolicyID])
GO

ALTER TABLE [dbo].[InsuranceByCustomer] CHECK CONSTRAINT [FK_InsuranceByCustomer_InsurancePolicy]