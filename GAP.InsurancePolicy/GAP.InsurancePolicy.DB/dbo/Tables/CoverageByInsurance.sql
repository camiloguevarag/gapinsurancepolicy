﻿CREATE TABLE [dbo].[CoverageByInsurance](
	[CoverageByInsuranceID] [int] IDENTITY(1,1) NOT NULL,
	[InsurancePolicyID] [int] NOT NULL,
	[CoverageTypeID] [int] NOT NULL,
	[Percentage] [decimal](18, 0) NOT NULL,
	[Risk] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_CoverageByInsurance_1] PRIMARY KEY CLUSTERED 
(
	[InsurancePolicyID] ASC,
	[CoverageTypeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[CoverageByInsurance]  WITH CHECK ADD  CONSTRAINT [FK_CoverageByInsurance_CoverageType] FOREIGN KEY([CoverageTypeID])
REFERENCES [dbo].[CoverageType] ([CoverageTypeID])
GO

ALTER TABLE [dbo].[CoverageByInsurance] CHECK CONSTRAINT [FK_CoverageByInsurance_CoverageType]
GO
ALTER TABLE [dbo].[CoverageByInsurance]  WITH CHECK ADD  CONSTRAINT [FK_CoverageByInsurance_InsurancePolicy] FOREIGN KEY([InsurancePolicyID])
REFERENCES [dbo].[InsurancePolicy] ([InsurancePolicyID])
GO

ALTER TABLE [dbo].[CoverageByInsurance] CHECK CONSTRAINT [FK_CoverageByInsurance_InsurancePolicy]