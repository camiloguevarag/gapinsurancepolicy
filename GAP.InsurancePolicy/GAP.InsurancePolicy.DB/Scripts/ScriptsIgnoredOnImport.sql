﻿
USE [GAPInsuranceDB]
GO

/****** Object:  Table [dbo].[CoverageByInsurance]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[CoverageType]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Customer]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[InsuranceByCustomer]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[InsurancePolicy]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[Role]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/****** Object:  Table [dbo].[User]    Script Date: 22/07/2018 13:21:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[CoverageByInsurance] ON
GO

INSERT [dbo].[CoverageByInsurance] ([CoverageByInsuranceID], [InsurancePolicyID], [CoverageTypeID], [Percentage], [Risk]) VALUES (2, 1, 2, CAST(30 AS Decimal(18, 0)), N'Bajo')
GO

INSERT [dbo].[CoverageByInsurance] ([CoverageByInsuranceID], [InsurancePolicyID], [CoverageTypeID], [Percentage], [Risk]) VALUES (1, 1, 3, CAST(100 AS Decimal(18, 0)), N'Medio')
GO

INSERT [dbo].[CoverageByInsurance] ([CoverageByInsuranceID], [InsurancePolicyID], [CoverageTypeID], [Percentage], [Risk]) VALUES (3, 2, 4, CAST(100 AS Decimal(18, 0)), N'Medio-alto')
GO

INSERT [dbo].[CoverageByInsurance] ([CoverageByInsuranceID], [InsurancePolicyID], [CoverageTypeID], [Percentage], [Risk]) VALUES (4, 2, 5, CAST(40 AS Decimal(18, 0)), N'Bajo')
GO

SET IDENTITY_INSERT [dbo].[CoverageByInsurance] OFF
GO

SET IDENTITY_INSERT [dbo].[CoverageType] ON
GO

INSERT [dbo].[CoverageType] ([CoverageTypeID], [Description]) VALUES (1, N'Terremoto')
GO

INSERT [dbo].[CoverageType] ([CoverageTypeID], [Description]) VALUES (2, N'Incendio')
GO

INSERT [dbo].[CoverageType] ([CoverageTypeID], [Description]) VALUES (3, N'Robo')
GO

INSERT [dbo].[CoverageType] ([CoverageTypeID], [Description]) VALUES (4, N'Incendio')
GO

INSERT [dbo].[CoverageType] ([CoverageTypeID], [Description]) VALUES (5, N'Incendio')
GO

SET IDENTITY_INSERT [dbo].[CoverageType] OFF
GO

SET IDENTITY_INSERT [dbo].[Customer] ON
GO

INSERT [dbo].[Customer] ([CustomerID], [FullName], [LastName], [Phone], [Identification]) VALUES (1, N'Cristian Camilo', N'Guevara', N'5774208', N'1128452600')
GO

SET IDENTITY_INSERT [dbo].[Customer] OFF
GO

SET IDENTITY_INSERT [dbo].[InsurancePolicy] ON
GO

INSERT [dbo].[InsurancePolicy] ([InsurancePolicyID], [Name], [Description], [StartDate], [CoverageTime], [Price]) VALUES (1, N'Seguro basico robo', N'Cubre el 100% en caso de robo', CAST(0x6B3E0B00 AS Date), 6, CAST(1000000.00 AS Decimal(18, 2)))
GO

INSERT [dbo].[InsurancePolicy] ([InsurancePolicyID], [Name], [Description], [StartDate], [CoverageTime], [Price]) VALUES (2, N'Poliza completa', N'cubre todo', CAST(0xD13E0B00 AS Date), 20, CAST(2000000.00 AS Decimal(18, 2)))
GO

SET IDENTITY_INSERT [dbo].[InsurancePolicy] OFF
GO

SET IDENTITY_INSERT [dbo].[Role] ON
GO

INSERT [dbo].[Role] ([RoleID], [Description]) VALUES (1, N'Administrator')
GO

INSERT [dbo].[Role] ([RoleID], [Description]) VALUES (2, N'Commercial')
GO

SET IDENTITY_INSERT [dbo].[Role] OFF
GO

SET IDENTITY_INSERT [dbo].[User] ON
GO

INSERT [dbo].[User] ([UserID], [UserName], [Password], [RoleID]) VALUES (1, N'camilo', N'camilogg15', 1)
GO

SET IDENTITY_INSERT [dbo].[User] OFF
GO
