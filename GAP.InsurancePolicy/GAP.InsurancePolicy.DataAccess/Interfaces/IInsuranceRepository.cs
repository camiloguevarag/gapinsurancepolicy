﻿using GAP.InsurancePolicy.Entities;

namespace GAP.InsurancePolicy.DataAccess.Interfaces
{
    public interface IInsuranceRepository : IGenericRepository<GAP.InsurancePolicy.Entities.InsurancePolicy>
    {

    }
}
