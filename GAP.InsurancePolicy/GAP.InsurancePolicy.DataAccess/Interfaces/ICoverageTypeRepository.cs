﻿using GAP.InsurancePolicy.Entities;

namespace GAP.InsurancePolicy.DataAccess.Interfaces
{
    public interface ICoverageTypeRepository : IGenericRepository<CoverageType>
    {

    }
}
