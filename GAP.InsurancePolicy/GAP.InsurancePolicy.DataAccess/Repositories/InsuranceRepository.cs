﻿using GAP.InsurancePolicy.DataAccess.Interfaces;

namespace GAP.InsurancePolicy.DataAccess.Repositories
{
    public class InsuranceRepository : GenericRepository<GAP.InsurancePolicy.Entities.GAPInsuranceDBEntities, GAP.InsurancePolicy.Entities.InsurancePolicy>, IInsuranceRepository
    {
    }
}
