﻿using GAP.InsurancePolicy.DataAccess.Interfaces;
using GAP.InsurancePolicy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.InsurancePolicy.DataAccess.Repositories
{
    public class UserRepository : GenericRepository<GAP.InsurancePolicy.Entities.GAPInsuranceDBEntities, User>, IUserRepository
    {

        public User GetUser(User user)
        {
            return GetAll().FirstOrDefault(x => x.UserName == user.UserName && x.Password == user.Password);
        }
    }
}
