﻿using GAP.InsurancePolicy.DataAccess.Interfaces;
using GAP.InsurancePolicy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.InsurancePolicy.DataAccess.Repositories
{
    public class CoverageByInsuranceRepository : GenericRepository<GAP.InsurancePolicy.Entities.GAPInsuranceDBEntities, CoverageByInsurance>, ICoverageByInsuranceRepository
    {

    }
}
