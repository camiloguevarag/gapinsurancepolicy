﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GAP.InsurancePolicy.Web.Startup))]
namespace GAP.InsurancePolicy.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
