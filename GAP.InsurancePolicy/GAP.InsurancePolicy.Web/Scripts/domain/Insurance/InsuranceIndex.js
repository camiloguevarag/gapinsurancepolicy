﻿var grid, gridCoverage, dialog, dialogCoverage, dataSourceInsurance;
var coveragesByInsurance = [], dataSourceCoverageByInsurence = [];
$(function () {

    dialog = $('#dialog').dialog({
        autoOpen: false,
        resizable: true,
        modal: true,
        width: 1000,
        scrollable: true,
    });

    dialogCoverage = $('#dialogCoverage').dialog({
        autoOpen: false,
        resizable: false,
        modal: true,
        width: 700
    });

    $('#startdate').datepicker();


    grid = $('#grid').grid({
        dataSource: dataSourceInsurance,
        columns: [
            { field: 'InsurancePolicyID', hidden: true },
            { field: 'Name', title: 'Nombre' },
            { field: 'Description', title: 'Descripción', sortable: true },
            { field: 'StartDate', title: 'Fecha inicio', type: 'date' },
            { field: 'Price', title: 'Precio', sortable: true },
            { field: 'CoverageTime', title: 'Período', sortable: true },
            { width: 64, tmpl: '<span class="material-icons gj-cursor-pointer">edit</span>', align: 'center', events: { 'click': EditInsurance } }

        ],
        pager: { limit: 10 }
    });

    getInsurance();

    $.ajax({
        url: '/CoverageType/GetCoverageTypeList',
        type: "GET",
        success: function (data) {            
            $.each(data, function (i, d) {                
                $('#coveragetype').append('<option value="' + d.CoverageTypeID + '">' + d.Description + '</option>');
            });
        }
    });

    gridCoverage = $('#gridCoverage').grid({
        dataSource: dataSourceCoverageByInsurence,
        columns: [
            { field: 'InsurancePolicyID', hidden: true },
            { field: 'CoverageTypeID', hidden: true, renderer: function (value, record) { return record.CoverageType.CoverageTypeID } },
            { field: 'Description', title: 'Cobertura', renderer: function (value, record) { return record.CoverageType.Description } },
            { field: 'Percentage', title: 'Porcentaje', sortable: true },
            { field: 'Risk', title: 'Riesgo', sortable: true }       

        ],
        pager: { limit: 2 }
    });

    $('#btnNewInsurance').on('click', function () {        
        $('#ID').val('0');
        $('#name').val('');
        $('#description').val('');
        $('#startdate').val('');
        $('#coveragetime').val('');
        $('#price').val('');
        $('#description').val('');
        dataSourceCoverageByInsurence = [];
        gridCoverage.render(dataSourceCoverageByInsurence);
        dialog.open();
    });

    $('#btnAddCoverage').on('click', function () {
        $('#alertInfo').css('display', 'none');
        dialogCoverage.open('Coberturas');
    });

    $('#btnSaveCoverage').on('click', SaveCoverage);
    $('#btnCancelCoverage').on('click', CancelCoverage);
    $('#btnSaveInsurance').on('click', SaveInsurance);
    $('#btnCancelInsurance').on('click', CancelInsurance);
});


function EditInsurance(e) {
    getCoverageByInsurance(e.data.record.InsurancePolicyID);
    $('#ID').val(e.data.record.InsurancePolicyID);
    $('#name').val(e.data.record.Name);
    $('#description').val(e.data.record.Description);
    $('#startdate').val(e.data.record.StartDate);
    $('#coveragetime').val(e.data.record.CoverageTime);
    $('#price').val(e.data.record.Price);
    $('#description').val(e.data.record.Description);

    dialog.open();
}

function ValidateRisk(percentage, risk) {
    if (percentage.trim() == "" || percentage < 0) {
        $('#alerInfolbl').text("Debe ingresar un porcentaje valido");
        $('#alertInfo').css('display', 'block')
        return false;
    }
    else {
        if (risk == "Alto" && percentage > 50) {
            $('#alerInfolbl').text("El porcentaje de cubrimiento no puede ser superior a 50% para un riesgo alto");
            $('#alertInfo').css('display', 'block')
            return false;
        }
    }
    return true;
}

function getCoverageByInsurance(id) {
    $.ajax({
        url: '/CoverageByInsurance/GetCoverageTypeList/' + id,
        type: "GET",
        success: function (data) {
            dataSourceCoverageByInsurence = data;
            gridCoverage.render(dataSourceCoverageByInsurence);
        }
    });
}

function getInsurance() {
    $.ajax({
        url: '/Insurance/GetInsuranceList',
        type: "GET",
        success: function (data) {
            dataSourceInsurance = data;
            grid.render(dataSourceInsurance);
        }
    });
}

function SaveInsurance() {
    var Insurance = {
        InsurancePolicyID: $('#ID').val(),
        Name: $('#name').val(),
        Description: $('#description').val(),
        StartDate: $('#startdate').val(),
        Price: $('#price').val(),
        CoverageTime: $('#coveragetime').val(),
    };
    Insurance.CoverageByInsurance = dataSourceCoverageByInsurence;

    $.ajax({ url: '/Insurance/Save', data: { record: Insurance }, method: 'POST' })
        .done(function () {
            dialog.close();
            getInsurance();
        })
        .fail(function () {
            alert('Failed to save.');
            dialog.close();
        });
}

function CancelInsurance() {
    dialog.close();
}

function SaveCoverage() {
    var coverageType = $('#coveragetype').val();
    var coverageTypeText = $('#coveragetype').find(":selected").text()
    var percentage = $('#percentage').val();
    var risk = $('#risk').val();

    if (!ValidateRisk(percentage, risk)) {
        return false;
    }

    var coverage = { CoverageByInsuranceID: 0, InsurancePolicyID: $('#ID').val(), Percentage: percentage, Risk: risk };
    coverage.CoverageType = { CoverageTypeID: coverageType, Description: coverageTypeText };
    dataSourceCoverageByInsurence.push(coverage);
    gridCoverage.render(dataSourceCoverageByInsurence);
    dialogCoverage.close();
}

function CancelCoverage() {
    dialogCoverage.close();
}

