﻿using GAP.InsurancePolicy.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GAP.InsurancePolicy.Web.Controllers
{
    public class CoverageByInsuranceController : Controller
    {
        private ICoverageByInsuranceBL _coverageByInsuranceBL;
        public CoverageByInsuranceController(ICoverageByInsuranceBL coverageByInsuranceBL)
        {
            this._coverageByInsuranceBL = coverageByInsuranceBL;
        }

        
        public ActionResult GetCoverageTypeList(int id)
        {
            var list = _coverageByInsuranceBL.getCoveragesByInsurance(id);
            return Json(list, JsonRequestBehavior.AllowGet);
        }
	}
}