﻿using GAP.InsurancePolicy.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GAP.InsurancePolicy.Web.Controllers
{
    [Authorize]
    public class InsuranceController : Controller
    {

        private IInsuranceBL _insuranceBL;

        public InsuranceController(IInsuranceBL insuranceBL)
        {
            this._insuranceBL = insuranceBL;
        }
        //
        // GET: /Insurance/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetInsuranceList()
        {
            var list = _insuranceBL.getList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCoverageByInsuranceList(int insurancePolicyId)
        {
            var list = _insuranceBL.getList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Save(GAP.InsurancePolicy.Entities.InsurancePolicy record)
        {
            var result = _insuranceBL.Save(record);
            return Json(new { result = true });
        }        

    }
}
