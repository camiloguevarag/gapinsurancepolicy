﻿using GAP.InsurancePolicy.BusinessLogic;
using GAP.InsurancePolicy.BusinessLogic.Interfaces;
using GAP.InsurancePolicy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GAP.InsurancePolicy.Web.Controllers
{
    [Authorize]
    public class CoverageTypeController : Controller
    {
        private ICoverageTypeBL _coverageTypeBL;
        public CoverageTypeController(ICoverageTypeBL coverageTypeBL)
        {
            this._coverageTypeBL = coverageTypeBL;
        }

        
        public ActionResult GetCoverageTypeList()
        {           
            var list = _coverageTypeBL.getList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }
      
    }
}
