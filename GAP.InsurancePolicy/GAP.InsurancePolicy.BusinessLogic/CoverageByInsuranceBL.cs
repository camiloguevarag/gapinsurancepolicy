﻿using GAP.InsurancePolicy.BusinessLogic.Interfaces;
using GAP.InsurancePolicy.DataAccess.Interfaces;
using GAP.InsurancePolicy.DataAccess.Repositories;
using GAP.InsurancePolicy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.InsurancePolicy.BusinessLogic
{
    public class CoverageByInsuranceBL : ICoverageByInsuranceBL
    {
        private ICoverageByInsuranceRepository coverageByInsuranceRepository;

        public CoverageByInsuranceBL()
        {
            coverageByInsuranceRepository = new CoverageByInsuranceRepository();
        }

        public List<CoverageByInsurance> getCoveragesByInsurance(int id)
        {
            return coverageByInsuranceRepository.GetAll().Where(x => x.InsurancePolicyID == id).ToList();
        }

        public bool Save(CoverageByInsurance coverageByInsurance)
        {
            try
            {
                coverageByInsuranceRepository.Add(coverageByInsurance);
                coverageByInsuranceRepository.Save();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
           
        }
    }
}
