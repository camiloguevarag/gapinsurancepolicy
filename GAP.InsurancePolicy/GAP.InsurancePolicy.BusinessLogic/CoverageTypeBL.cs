﻿using GAP.InsurancePolicy.BusinessLogic.Interfaces;
using GAP.InsurancePolicy.DataAccess.Interfaces;
using GAP.InsurancePolicy.DataAccess.Repositories;
using GAP.InsurancePolicy.Entities;
using System.Collections.Generic;
using System.Linq;

namespace GAP.InsurancePolicy.BusinessLogic
{
    public class CoverageTypeBL : ICoverageTypeBL
    {
        private ICoverageTypeRepository coverageTypeRepository;

        public CoverageTypeBL()
        {
            coverageTypeRepository = new CoverageTypeRepository();
        }

        public List<CoverageType> getList()
        {
            return coverageTypeRepository.GetAll().ToList();
        }
    }
}
