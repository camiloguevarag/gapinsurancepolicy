﻿using GAP.InsurancePolicy.BusinessLogic.Interfaces;
using GAP.InsurancePolicy.DataAccess.Interfaces;
using GAP.InsurancePolicy.DataAccess.Repositories;
using GAP.InsurancePolicy.Entities;
using System.Collections.Generic;
using System.Linq;

namespace GAP.InsurancePolicy.BusinessLogic
{
    public class InsuranceBL : IInsuranceBL
    {
         private IInsuranceRepository _insuranceRepository;
         private ICoverageByInsuranceBL _coverageByInsuranceBL;

         public InsuranceBL(ICoverageByInsuranceBL coverageByInsuranceBL)
        {
            _insuranceRepository = new InsuranceRepository();
            this._coverageByInsuranceBL = coverageByInsuranceBL;
        }
    
        public List<GAP.InsurancePolicy.Entities.InsurancePolicy> getList()
        {
            return _insuranceRepository.GetAll().ToList();
        }

        public bool Save(GAP.InsurancePolicy.Entities.InsurancePolicy insurance)
        {
            try
            {
                if (insurance.InsurancePolicyID != null && insurance.InsurancePolicyID != 0)
                {
                    var newCoverageList = insurance.CoverageByInsurance.Where(x => x.CoverageByInsuranceID == 0).ToList();
                    foreach (var coverage in newCoverageList)
                    {
                        insurance.CoverageByInsurance.Remove(coverage);
                    }  

                    _insuranceRepository.Edit(insurance);
                    //if (insurance.CoverageByInsurance.Where(x => x.CoverageByInsuranceID == 0).Any())
                    //{                         
                        foreach(var coverage in newCoverageList)
                        {
                            _coverageByInsuranceBL.Save(coverage);
                        }                        
                    //}
                }
                else
                {
                    _insuranceRepository.Add(insurance);                    
                }
                _insuranceRepository.Save();
                return true;
            }
            catch (System.Exception ex)
            {
                return false;
            }
           
           
        }

    }
}
