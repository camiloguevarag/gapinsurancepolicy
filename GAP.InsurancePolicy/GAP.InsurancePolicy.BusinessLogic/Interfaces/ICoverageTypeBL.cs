﻿using GAP.InsurancePolicy.Entities;
using System.Collections.Generic;

namespace GAP.InsurancePolicy.BusinessLogic.Interfaces
{
    public interface ICoverageTypeBL
    {
        List<CoverageType> getList();
    }
}
