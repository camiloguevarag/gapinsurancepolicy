﻿using GAP.InsurancePolicy.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GAP.InsurancePolicy.BusinessLogic.Interfaces
{
    public interface ICoverageByInsuranceBL
    {
        List<CoverageByInsurance> getCoveragesByInsurance(int id);

        bool Save(CoverageByInsurance coverageByInsurance);
    }
}
