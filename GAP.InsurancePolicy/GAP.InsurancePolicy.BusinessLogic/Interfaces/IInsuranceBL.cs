﻿using System.Collections.Generic;

namespace GAP.InsurancePolicy.BusinessLogic.Interfaces
{
    public interface IInsuranceBL
    {
        List<GAP.InsurancePolicy.Entities.InsurancePolicy> getList();
        bool Save(GAP.InsurancePolicy.Entities.InsurancePolicy insurance);
    }
}
